/* Core Logic */
$(function() {
    $().dndPageScroll();

    /* 3rd place */

    $('#3rd').hide()

    $('.3rd').on('click', function() {
        $('#3rd').toggle();
    })

    /* Player Button */

    $('#players button').on('click', function(e) {
        var flag = $('#flag').val();
        var name = $('#name').val();
        var race = $('#race').val();

        if ($(this).html() == 'Add') {
            /* Add */
            if (name == '' || name == 'BYE') {
                alert('Invalid name!');
                $('#name').focus();
                return false;
            }

            var id = $.now();

            $('#players ul').append('<li id="' + id + '" draggable="true"><img src="flags/' + flag + '.png" /> ' + name + '</li>');
            $('#' + id).addClass(race);

            var counter = parseInt($('#players h1 span').html()) + 1;

            $('#players h1 span').html(counter);

            if (counter == Math.pow(2, $('#rounds').val()) + 1) {
                $('#players h1 span').addClass('warn');
            }

            if ($('p').hasClass(id)) {
                $('p .' + id).remove()
            }
        } else {
            /* Edit */
            if (name == '' || name == 'BYE') {
                alert('Invalid name!');
                $('#name').focus();
                return false;
            }

            var id = $('#players button').attr('class');

            $('#' + id).html('<img src="flags/' + flag + '.png" /> ' + name).removeClass().addClass(race);

            $(this).html('Add').removeClass();

            if ($('p').hasClass(id)) {
                $('p.' + id).each(function() {
                    var original = $(this).hasClass('top') ? 'top' : 'bottom';
                    var bold = $(this).hasClass('bold') ? 'bold' : '';
                    var value = $(this).children('input').val();

                    $(this).html('<img src="flags/' + flag + '.png" /> ' + name + '<input value="' + value + '" />').removeClass().addClass(original + ' ' + race + ' ' + id + ' ' + bold);
                })
            }
        }

        /*
		$('#race').val('n');
		$('#name').val('').focus();
		*/
    })

    /* Drag and Drop controls */

    /* Players */
    $('#players').on('dragstart', 'li', function(e) {
        $(this).addClass('drag')
        var id = ($.type($(this).attr('id')) == 'undefined') ? '0' : $(this).attr('id');

        e.originalEvent.dataTransfer.setData('data', 'player;' + id);
    })

    $('#players').on('dragend', 'li', function(e) {
        $(this).removeClass('drag');
    })

    /* --- */

    $('.edit, .remove, p.top, p.bottom').on('dragover', function(e) {
        e.preventDefault();
    })

    $('.edit, .remove, p.top, p.bottom').on('dragenter', function(e) {
        e.preventDefault();
        $(this).addClass('enter');
    })

    $('.edit, .remove, p.top, p.bottom').on('dragleave', function(e) {
        $(this).removeClass('enter');
    })

    /* Edit Player */

    $('#players .edit').on('drop', function(e) {
        e.preventDefault();
        var data = e.originalEvent.dataTransfer.getData('data').split(';');
        var type = data[0];

        if (type == 'player') {
            var id = data[1];

            if (id > 1) {
                var flag = $('#' + id + ' img').attr('src').split('/')[1].split('.')[0];
                var name = $('#' + id).text().trim();
                var race = $('#' + id).attr('class').split(' ')[0];

                $('#flag').val(flag);
                $('#name').val(name);
                $('#race').val(race);

                $('#players button').removeClass().html('Edit').addClass(id);
            }
        }

        $(this).removeClass('enter');
    })

    /* Remove Player */

    $('#players .remove').on('drop', function(e) {
        e.preventDefault();
        var data = e.originalEvent.dataTransfer.getData('data').split(';');
        var type = data[0];

        if (type == 'player') {
            var id = data[1];

            if (id > 1) {
                if ($('#players button').hasClass(id)) {
                    alert("You are editing this player.");
                    $(this).removeClass('enter');
                    return false
                }

                var game = $('.' + id).first().attr('id');

                removePlayer(game, id)

                var original = $(this).hasClass('top') ? 'top' : 'bottom';

                $('#' + game).html('<img src="flags/ff.png" /> <input value="" />').removeClass().addClass(original + ' n');

                $('#' + id).remove()

                var counter = parseInt($('#players h1 span').html()) - 1;

                $('#players h1 span').html(counter);

                if (counter == Math.pow(2, $('#rounds').val())) {
                    $('#players h1 span').removeClass('warn');
                }
            }
        }

        $(this).removeClass('enter');
    })

    /* Brackets */

    $('p.top, p.bottom').on('drop', function(e) {
        e.preventDefault();
        var data = e.originalEvent.dataTransfer.getData('data').split(';');
        var type = data[0];

        if (type == 'player') {
            var id = parseInt(data[1]);
            var original = $(this).hasClass('top') ? 'top' : 'bottom';

            if (id) {
                if (id == 1) {
                    var bracketId = ($.type($(this).attr('class').split(' ')[2]) == 'undefined') ? 0 : parseInt($(this).attr('class').split(' ')[2]);

                    if (bracketId) {
                        var bracketGame = $(this).attr('id');
                        removePlayer(bracketGame, bracketId);

                        var original = $(this).hasClass('top') ? 'top' : 'bottom';

                        $(this).html('<img src="flags/ff.png" /> <input value="" />').removeClass().addClass(original + ' n');

                        if ($('.' + bracketId).length > 0) {
                            $('.' + bracketId).eq(-1).removeClass('bold')
                        }
                    }
                } else {
                    if ($('p').hasClass(id)) {
                        alert("Player already on brackets!");
                        $(this).removeClass('enter');
                        return false;
                    }

                    var flag = $('#' + id + ' img').attr('src').split('/')[1].split('.')[0];
                    var name = $('#' + id).text().trim();
                    var race = $('#' + id).attr('class').split(' ')[0];

                    $(this).html('<img src="flags/' + flag + '.png" /> ' + name + '<input value="" />').removeClass().addClass(original + ' ' + race + ' ' + id);
                }
            } else {
                $(this).html('BYE <input value="-" />').removeClass().addClass(original + ' bye');
            }

            var game = $(this).attr('id');

            matchRace(game);
			matchFlag(game);
        }
    })

    $('.top, .bottom').on('click', function() {
        var id = ($.type($(this).attr('class').split(' ')[2]) == 'undefined') ? 0 : parseInt($(this).attr('class').split(' ')[2]);

        if (id) {
            var game = $(this).attr('id');

            if ($(this).hasClass('bold')) {
                $(this).removeClass('bold');

                removePlayer(game, id)
            } else {
                $(this).addClass('bold');

                if ($(this).siblings('p').hasClass('bold')) {
                    $(this).siblings('p').removeClass('bold');

                    var rivalId = $(this).siblings('p').attr('class').split(' ')[2];
                    var rivalGame = $(this).siblings('p').attr('id');

                    removePlayer(rivalGame, rivalId);
                }

                var flag = $('#' + id + ' img').attr('src').split('/')[1].split('.')[0];
                var name = $('#' + id).text().trim();
                var race = $('#' + id).attr('class').split(' ')[0];

                var data = /R(\d+)[D|W](\d+)/.exec(game);

                if (data[1] != $('#rounds').val()) {
                    var next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2)
                    var original = $('#' + next).hasClass('top') ? 'top' : 'bottom';

                    $('#' + next).html('<img src="flags/' + flag + '.png" /> ' + name + '<input value="" />').removeClass().addClass(original + ' ' + race + ' ' + id);
                }
            }
        }
    })

    $('.top input, .bottom input').on('click', function(e) {
        e.preventDefault();
    })

    /* Export */

    $('#export').on('click', function() {
        $('textarea').val();

        var rounds = $('#rounds').val();
        var game = $('p.top, p.bottom');
        var k = 0
        var headers = ''
        var header = ''
        var text = ''

        for (var i = rounds; i > 0; i--) {
            var games = Math.pow(2, i)

            var h = (parseInt(rounds) + 1) - i;

            if (games == 8) {
                headers = 'Quarterfinals'
            } else if (games == 4) {
                headers = 'Semifinals'
            } else if (games == 2) {
                headers = 'Finals'
            } else {
                headers = 'Round of ' + games
            }

            if ($('#R' + h + 'H input').val() != headers) {
                header = header + '\n|R' + h + '= ' + $('#R' + h + 'H input').val()
            }

            text = text + '\n<!-- ' + headers + ' -->\n';

            for (var j = 1; j <= games; j++) {
                var id = $(game[k]).attr('id');
                var flag = $(game[k]).children('img').attr('src').split('/')[1].split('.')[0];
                var name = $(game[k]).text().trim();
                var race = $(game[k]).attr('class').split(' ')[1];
                var score = $(game[k]).children('input').val().trim();
                var win = $(game[k]).hasClass('bold') ? '1' : '';

                text = text + '|' + id + '=' + name + ' |' + id + 'race=' + (race == 'n' ? '' : race) + ' |' + id + 'flag=' + (flag == 'ff' ? '' : flag) + ' |' + id + 'score=' + score + ' |' + id + 'win=' + win + '\n';

                k++;
            }
        }

        if (!$('#3rd').is(':hidden')) {
            text = text + '\n<!-- 3rd Place Match -->\n';

            for (var j = 1; j <= 2; j++) {
                var id = $(game[k]).attr('id');
                var flag = $(game[k]).children('img').attr('src').split('/')[1].split('.')[0];
                var name = $(game[k]).text().trim();
                var race = $(game[k]).attr('class').split(' ')[1];
                var score = $(game[k]).children('input').val().trim();
                var win = $(game[k]).hasClass('bold') ? '1' : '';

                text = text + '|' + id + '=' + name + ' |' + id + 'race=' + (race == 'n' ? '' : race) + ' |' + id + 'flag=' + (flag == 'ff' ? '' : flag) + ' |' + id + 'score=' + score + ' |' + id + 'win=' + win + '\n';

                k++;
            }
        }

        text = text + '\n}}';

        if (header.length) {
            header = header + '\n'
        }

        $('textarea').val('{{' + Math.pow(2, rounds) + 'SEBracket\n' + header + text);
    })
})

function removePlayer(game, id) {
    var original = '';

    var data = /R(\d+)[D|W](\d+)/.exec(game);
    var next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2);
    var round = parseInt(data[1]) + 1;

    while ($('#' + next).hasClass(id) && round <= $('#rounds').val()) {
        original = $('#' + next).hasClass('top') ? 'top' : 'bottom';

        $('#' + next).html('<img src="flags/ff.png" /> <input value="" />').removeClass().addClass(original + ' n');

        data = /R(\d+)[D|W](\d+)/.exec(next);
        next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2);

        round = parseInt(data[1]) + 1;
    }
}

function matchRace(game) {
    var rounds = $('#rounds').val();
	
	var race = $('#' + game).attr('class').split(' ')[1]
	var rivalRace = $('#' + game).siblings('p').attr('class').split(' ')[1];
	
	var data = /R(\d+)[D|W](\d+)/.exec(game);
	var next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2);
	var round = parseInt(data[1]) + 1;
	
	var original = ''
	
	while (race == rivalRace && round <= rounds) {
		original = $('#' + next).hasClass('top') ? 'top' : 'bottom';
		
		$('#' + next).removeClass().addClass(original + ' ' + race)
		
		race = $('#' + game).attr('class').split(' ')[1]
        rivalRace = $('#' + next).siblings('p').attr('class').split(' ')[1];
		
        data = /R(\d+)[D|W](\d+)/.exec(next);
        next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2);
        round = parseInt(data[1]) + 1;
    }
}

function matchFlag(game) {
    var rounds = $('#rounds').val();
	
	var flag = $('#' + game).children('img').attr('src').split('/')[1].split('.')[0];
	var rivalFlag = $('#' + game).siblings('p').children('img').attr('src').split('/')[1].split('.')[0];
	
	var data = /R(\d+)[D|W](\d+)/.exec(game);
	var next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2);
	var round = parseInt(data[1]) + 1;
	
	while (flag == rivalFlag && round <= rounds) {
		$('#' + next + ' img').attr('src', 'flags/' + flag + '.png')
		
		flag = $('#' + next).children('img').attr('src').split('/')[1].split('.')[0];
        rivalFlag = $('#' + next).siblings('p').children('img').attr('src').split('/')[1].split('.')[0];
		
        data = /R(\d+)[D|W](\d+)/.exec(next);
        next = 'R' + (parseInt(data[1]) + 1) + 'W' + Math.floor((parseInt(data[2]) + 1) / 2);
        round = parseInt(data[1]) + 1;
    }
}